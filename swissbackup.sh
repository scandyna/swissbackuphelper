#!/bin/bash

REMOTE=swissbackup
REMOTE_ROOT=$USER

LOCAL_MOUNT_DIRECTORY=~/media/$REMOTE

runBackup()
{
  directories=(
    Backups
    Documents
    mdtcablelist
    MetrisoCommanderData
    configs
    programmation/doc
    dev/MdtDeployUtils
  )

  for directory in "${directories[@]}"; do
    echo "backup ~/$directory to $REMOTE:$REMOTE_ROOT/$directory"
    rclone sync ~/"$directory" "$REMOTE:$REMOTE_ROOT/$directory" -v --stats 30s
  done
}

listBackup()
{
  echo "list $REMOTE:$REMOTE_ROOT ..."
  rclone lsd "$REMOTE:$REMOTE_ROOT"
  rclone lsl "$REMOTE:$REMOTE_ROOT" --max-depth 1
}

mountBackup()
{
  echo "create $LOCAL_MOUNT_DIRECTORY ..."
  mkdir -p "$LOCAL_MOUNT_DIRECTORY"

  echo "mounting $REMOTE:$REMOTE_ROOT to $LOCAL_MOUNT_DIRECTORY"
  rclone mount "$REMOTE:$REMOTE_ROOT" "$LOCAL_MOUNT_DIRECTORY" &
}

umountBackup()
{
  echo "umount $LOCAL_MOUNT_DIRECTORY ..."

  fusermount -u "$LOCAL_MOUNT_DIRECTORY"
}

promptAboutAction()
{
  echo -n "What to do ? [b:backup, ls:list backups, mount:mount to local dir, umount:umount local dir]: "
  read answer

  case $answer in
    b)
      runBackup
      ;;
    ls)
      listBackup
      ;;
    mount)
      mountBackup
      ;;
    umount)
      umountBackup
      ;;
    *)
      echo "Unknown action"
      exit 1
      ;;
  esac
}

promptAboutAction
